## Bayesian Optimization

This repository contains the solution and implementation for the group 
assignment of the Data Mining course (JADS 2019-2020). 

The specification of the assignment can be found in the file named 
*(assignment-specification)Assignment 2 - Bayesian optimization.ipynb*. 

The solution developed with detailed description is named *Assignment2_Group1* 
and can be found in the .html, .ipynd, and .pdf formats.

