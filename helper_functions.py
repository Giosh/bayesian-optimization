import openml as oml
from sklearn.ensemble import RandomForestRegressor
import numpy as np
from scipy.stats import norm
from sklearn.model_selection import cross_val_score
from sklearn import ensemble
from sklearn.linear_model import ElasticNet
from sklearn.svm import SVC
import random as rd
import warnings
warnings.filterwarnings("ignore")
from copy import deepcopy, copy
from pylab import rcParams
import matplotlib.ticker as mticker
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, Matern

MODEL_TO_LABELS = {'SVM': ('C', 'Gamma'), 'Gradient Boosting Classifier': ('learning_rate', 'max_depth'),'Elastic Net': ('alpha', 'l1_ratio'),'Gradient Boosting Regressor': ('learning_rate', 'max_depth')}
rcParams['figure.figsize'] = 20, 5


# Random Forest that also returns the standard deviation of predictions
class ProbabilisticRandomForestRegressor(RandomForestRegressor):
    """
    A Random Forest regressor that can also returns the standard deviations for all predictions
    """
    def predict(self, X, return_std=True):       
        preds = []
        for pred in self.estimators_:
            preds.append(pred.predict(X))
        if return_std:
            return np.mean(preds, axis=0), np.std(preds, axis=0)
        else:
            return np.mean(preds, axis=0)
        
        
def EI(m, s, curr_best=0.0, balance=0.0, **kwargs):
    """Computes the Expected Improvement
    surrogate, The surrogate model
    X: np.ndarray(N, D), The input points where the acquisition function
        should be evaluated. N configurations with ​kwargsD hyperparameters
    curr_best, The current best performance
    balance, Decrease to focus more on exploration, increase to focus on exploitation (optional)
    Returns
    -------
    np.ndarray(N,1), Expected Improvement of X
    """

    z = (curr_best - m - balance) / s
    f = (curr_best - m - balance) * norm.cdf(z) + s * norm.pdf(z)
    
    if np.any(s == 0.0): # uncertainty should never be exactly 0.0
        f[s == 0.0] = 0.0

    return f


def diff(first, second):
    '''Compute difference between list of lists'''
    first_set = set(map(tuple, first))
    secnd_set = set(map(tuple, second))
    difference = list(first_set.symmetric_difference(secnd_set))
    return([list(elem) for elem in difference])


def sample_loss(X, y, model, params, scoring='accuracy'):
    '''Sample loss of SVM using 3-fold cross-validation. We can use either misclassification 
    error or mean squared error as the returned evaluation measure.'''
    if model == 'SVM':
        clf = SVC(C = params[0], 
                  gamma= params[1])
    elif model == 'Gradient Boosting Classifier':
        clf = ensemble.GradientBoostingClassifier(learning_rate = params[0], 
                                                  max_depth= params[1], 
                                                  n_estimators = 1000)
    elif model == 'Elastic Net':
        clf = ElasticNet(alpha = params[0], 
                         l1_ratio = params[1])
    elif model == 'Gradient Boosting Regressor':
        clf = ensemble.GradientBoostingRegressor(learning_rate = params[0], 
                                                 max_depth= params[1], 
                                                 n_estimators = 1000)
    score = cross_val_score(clf, X, y, scoring=scoring, cv=3).mean()
    return 1-score if scoring == 'accuracy' else score*(-1)
    

def initial_function_hyperparams(X,y,model, domain, n_first=10, scoring='accuracy'):
    '''It creates the first set of hyperparameters for the prior'''
    rd.seed(10)
    hyperparameters = []
    y_surr = []
    for i in range(n_first):
        params = [rd.choice(domain[:,0]),rd.choice(domain[:,1])]
        hyperparameters.append(params)
        y_surr.append(sample_loss(X, y, model, params, scoring=scoring))
    return hyperparameters, y_surr

    
def train_surrogate_model(initial_hp, params_space, surrogate):
    '''Trains the surrogate model with the prior info (10 configurations) and predicts all other configurations'''
    x_train = np.array(initial_hp[0])
    y_train = np.array(initial_hp[1])
    # Train surrogate model
    surrogate.fit(x_train, y_train) 
    # Predict values
    y_pred, sigma = surrogate.predict(params_space, return_std=True)
    return surrogate, y_pred, sigma
    

def filter_space(fixed_param, param_idx, params_space, initial_hp, sigma, y_pred, ei):
    x_train = np.array(initial_hp[0])
    y_train = np.array(initial_hp[1])

    fixed_param_space_idx = [fixed_param == params[param_idx] for params in params_space]
    fixed_param_space = params_space[fixed_param_space_idx]
    y_pred_fix_param = y_pred[fixed_param_space_idx]
    sigma_fix_param = sigma[fixed_param_space_idx]
    ei_fix_param = ei[fixed_param_space_idx]
    
    results = {'param_space':fixed_param_space, 'y_pred':y_pred_fix_param, 'sigma':sigma_fix_param, 'x_train': x_train, 'y_train':y_train, 'ei': ei_fix_param}
    return results


def fun_plot_surrogate_EI_2d(fixed_param, model, param_idx, fixed_param_values, n_iter):

    #fixed_param_idx is either 0 or q, if 0 it means that thfirst of the two hyperparameters
    #of a model is fixed and the second varies, if 1 the second one is fixed and the 0-th varies
    varying_param_idx=1 if param_idx==0 else 0
    x_points=np.sort(fixed_param_values['param_space'][:,varying_param_idx])
    y_pred=fixed_param_values['y_pred'][np.argsort(fixed_param_values['param_space'][:,varying_param_idx])]
    sigma=fixed_param_values['sigma'][np.argsort(fixed_param_values['param_space'][:,varying_param_idx])]
    
    # Plot surrogate model
    plt.subplot(2, 1, 1)
    plt.title("{} with fixed {} at {}".format(MODEL_TO_LABELS.get(model)[varying_param_idx], MODEL_TO_LABELS.get(model)[param_idx], fixed_param))
    plt.ylabel('Surrogate model error')
    #Plot new hyperparameter setting only if it's not the first iteration
    if(n_iter==0):
        plt.plot(fixed_param_values['x_train'][:,varying_param_idx], fixed_param_values['y_train'], 'r.', markersize=10, label=u'Observations');
    else:
        plt.plot(fixed_param_values['x_train'][:-1,varying_param_idx], fixed_param_values['y_train'][:-1], 'r.', markersize=10, label=u'Observations');
        plt.plot(fixed_param_values['x_train'][-1,varying_param_idx], fixed_param_values['y_train'][-1], 'go-', markersize=10, label=u'New Observation');
    
    plt.plot(x_points, y_pred, 'b-', label=u'Prediction');
    plt.fill_between(x_points.ravel(),y_pred-2*sigma,y_pred+2*sigma,alpha=0.1,label='Uncertainty');
    plt.xscale('log')
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5));

    # Plot acquisition function
    plt.subplot(2, 1, 2)
    plt.xlabel('Hyperparameter {} value'.format(MODEL_TO_LABELS.get(model)[varying_param_idx]));
    plt.ylabel('Expected Improvement')
    plt.plot(x_points, fixed_param_values['ei'][np.argsort(fixed_param_values['param_space'][:,varying_param_idx])])
    plt.xscale('log')
    plt.show()
    
    
def fun_plot_EI_3d(ei_space, ei, model):
    '''Computing EI and plotting it. Attention, the plot with 3d did not accept logscale of the axis, this is why we had to use a workaround. 
    The logarithmic scaled values are shown in the following way 3*exp(12) --> 1.00e+12'''
    x_label = MODEL_TO_LABELS.get(model)[0]
    y_label = MODEL_TO_LABELS.get(model)[1]
    
    x_plot = ei_space[:,0]
    y_plot = ei_space[:,1]
    
    plt.figure(figsize=(5, 5))
    ax = plt.axes(projection='3d')
    if(y_label == 'Gamma'):
        y_plot = np.log10(y_plot)
        ax.yaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
    
    ax.scatter3D(np.log10(x_plot), y_plot, ei, c=ei)
    ax.xaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
    ax.view_init(20, 50)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    ax.set_zlabel('EI')
    plt.show()
    

def log_tick_formatter(val, pos=None):
    return "{:.2e}".format(10**val)
    

class BayesianOptimizer():
    
    def __init__(self, n_iters, model, surrogate, initial_hyperparams_eval, params_space):
        self.n_iters=n_iters
        self.model=model
        self.surrogate=surrogate
        self.initial_hyperparams_eval=deepcopy(initial_hyperparams_eval)
        self.param_space=params_space
        
        self.score_change=0
        self.hyper_params=deepcopy(initial_hyperparams_eval[0])
        self.y_surr=deepcopy(initial_hyperparams_eval[1])        
        
        self.hyperparameters_eval_history=[]
        self.search_space_history=[]
        self.y_pred_history=[]
        self.sigma_history=[]
        self.ei_history=[]
        
        
    def optimize(self, X, y, scoring='accuracy'):
    
        for i in range(self.n_iters+1):
            self.hyperparameters_eval_history.append(deepcopy([self.hyper_params, self.y_surr]))
            
            #1. remove hyperparams already evaluated from the params space
            search_space=np.array(diff(self.param_space, self.hyper_params))
            self.search_space_history.append(deepcopy(search_space))
            
            #2.Train Surrogate model, and predict the expected performance 
            #of (unseen) hyperparameter values
            surrogate_fit_model, y_pred, sigma=train_surrogate_model([self.hyper_params, self.y_surr], search_space, self.surrogate)
            self.y_pred_history.append(copy(y_pred))
            self.sigma_history.append(copy(sigma))
            
            #3. Calculate Acquisition Function (Expected Improvement)
            ei = EI(y_pred, sigma, curr_best = np.amin(self.y_surr)) 
            self.ei_history.append(copy(ei))
            
            #4. Take the maximum of EI 
            idx_max = np.argmax(ei)
            params = search_space[idx_max]
            
            #5. Use maximum to evaluate new params configuration
            model_score = sample_loss(X, y, self.model, params, scoring=scoring)
           
            self.hyper_params.append(list(params))
            self.y_surr.append(model_score)

        self.score_change=((np.min(self.y_surr)-np.min(self.initial_hyperparams_eval[1]))/abs(np.min(self.initial_hyperparams_eval[1])))*100

            
    def plot_surrogate_EI_2d(self, fixed_params, iter_indexes):

        for i in iter_indexes: 
            if i==0:
                print("\n-------------Initialization--------------\n")
            else:
                print("\n-------------Plots at iteration n. {}--------------\n".format(i))
                      
            for j in range(2):
                fix_param_space=filter_space(fixed_params[j], j, self.search_space_history[i], self.hyperparameters_eval_history[i], self.sigma_history[i], self.y_pred_history[i], self.ei_history[i])
                fun_plot_surrogate_EI_2d(fixed_params[j], self.model, j, fix_param_space, i)
         
           
    def plot_surrogate_2d(self, fixed_params, iter_indexes):
    
        for i in iter_indexes:            
            #plot 2d slices
            for j in range(2):
                fix_param_space=filter_space(fixed_params[j], j, self.search_space_history[i], 
                self.hyperparameters_eval_history[i], self.sigma_history[i], self.y_pred_history[i], self.ei_history[i])
                
                fun_plot_surrogate_2d(fixed_params[j], self.model, j, fix_param_space)
    
    def plot_hyperparameters(self, iter_indexes):
        
        for i in iter_indexes:
            plot_results_BO(self.model, self.hyperparameters_eval_history[i], self.hyperparameters_eval_history[i][1])
            plt.show()
    
    
    def plot_EI_3d(self, iter_indexes):
        for i in iter_indexes:
                fun_plot_EI_3d(self.search_space_history[i], self.ei_history[i], self.model)  
        
    def get_best_parameters(self, fixed_params, plot_control=False):
        best_score_idx=np.argmin(self.y_surr)
        if(best_score_idx<=(len(self.initial_hyperparams_eval[0])-1)):
            print("The best parameters configuration is in the initialization")
            print("Best score: {} at \n{}={} and \n{}={}\n".format(self.y_surr[best_score_idx], MODEL_TO_LABELS.get(self.model)[0],self.hyper_params[best_score_idx][0], MODEL_TO_LABELS.get(self.model)[1],self.hyper_params[best_score_idx][1]))
            return [self.y_surr[best_score_idx], self.hyper_params[best_score_idx]]
        else:
            best_iter_idx=best_score_idx-len(self.initial_hyperparams_eval[0])
            print("Bayesian Optimization yielded better results at iteration n. {}".format(best_iter_idx+1))
            print("Best score: {} at \n{}={} and \n{}={}\n".format(self.y_surr[best_score_idx], MODEL_TO_LABELS.get(self.model)[0],self.hyper_params[best_score_idx][0], MODEL_TO_LABELS.get(self.model)[1],self.hyper_params[best_score_idx][1]))
            if(plot_control):
                self.plot_surrogate_EI_2d(fixed_params, [best_iter_idx])
                self.plot_EI_3d([best_iter_idx])
            return [self.y_surr[best_score_idx], self.hyper_params[best_score_idx]]     

        
    @property
    def get_10_best_configerations(self):
        best_params = sorted(list(zip(self.hyper_params, self.y_surr)), key = lambda x: x[1])[0:10]
        hyperparameters, y_surr = list(zip(*best_params))
        return list(hyperparameters), list(y_surr)
